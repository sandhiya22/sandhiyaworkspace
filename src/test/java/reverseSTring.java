import java.util.Scanner;

public class reverseSTring {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
String str;
System.out.println("enter the string:");
Scanner sc=new Scanner(System.in);

str=sc.nextLine();
sc.close();

String reversed=reverseString(str);

System.out.println("the reverse string is:"+reversed);

	}


	private static String reverseString(String str) {
		// TODO Auto-generated method stub
		{
			
			if (str.isEmpty() && str.length()<=1)
				return str;
			
			/*picks the character at the first position using str.charAt(0)
			  and puts it at the end of the resulting string, and then calls itself on the remainder using substring(1),
			  addition of these 2 will return the reversed string
*/			
	           return reverseString(str.substring(1))+str.charAt(0);
		}
	}
}