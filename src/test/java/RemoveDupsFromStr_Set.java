import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class RemoveDupsFromStr_Set {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String str;
		System.out.println("enter a string with duplicate characters in it:");
		Scanner sc=new Scanner(System.in);

		str=sc.nextLine();
		sc.close();
		
		char[] strArray= str.toCharArray();
		Set<Character> set=new LinkedHashSet<>();
		for(int i=0;i<strArray.length;i++)
		{
			set.add(strArray[i]);
		}
for (Character ch:set) {
	System.out.println("String without duplicates:"+ch);
}
	}

}
