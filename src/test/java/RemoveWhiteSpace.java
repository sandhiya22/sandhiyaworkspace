import java.util.Scanner;

public class RemoveWhiteSpace {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//initialising string variable (str) and getting string from runtime
		String str;
		System.out.println("Enter a line with white spaces:");
		Scanner sc=new Scanner(System.in);
		str=sc.nextLine();
		sc.close();
		
		//passing string to char array to read the length
		char [] strArray = str.toCharArray();
		
		//Calling string buffer class to manipulate the string
		StringBuffer sb = new StringBuffer();
							
		for (int i=0;i<strArray.length;i++) {
			if (strArray[i]!=' ' && strArray[i]!='\t' ){
				sb.append(strArray[i]);		
			}
			
		}
		System.out.println(sb);

	}

}
