import java.util.Scanner;

public class reverseString_iterator {

	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		
		String str;
		
		System.out.println("enter the string:");
		
		Scanner sc=new Scanner(System.in);
		
		str=sc.nextLine();
		 
		//pass string to char array to iterate
		char[] strArray = str.toCharArray();
		 
		//iterate the array in reverse order
		for (int i = strArray.length - 1; i >= 0; i--)
			
		{
			
		    System.out.print(strArray[i]);
		    
	    }

}
}