package week3.day1homeworks;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Createlead_allfields {

	//Question: Create a lead with all fields without the parent account and birth date field
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver createlead=new ChromeDriver();
		createlead.manage().window().maximize();
		//To open the URL
		createlead.get("http://leaftaps.com/opentaps");
		//enter the username
		createlead.findElementById("username").sendKeys("DemoSalesManager");
		//enter the password
		createlead.findElementById("password").sendKeys("crmsfa");
		//To click login
		createlead.findElementByClassName("decorativeSubmit").click();
		//To open crm/sfa
		createlead.findElementByLinkText("CRM/SFA").click();
		// To open Create Lead
		createlead.findElementByLinkText("Create Lead").click();
		//To enter company name
		Thread.sleep(2000);
		createlead.findElementById("createLeadForm_companyName").sendKeys("Edunex1"); 
		//To enter First name
		createlead.findElementById("createLeadForm_firstName").sendKeys("Roman1");
		// To enter last name
		createlead.findElementById("createLeadForm_lastName").sendKeys("Reigns1");
		WebElement source = createlead.findElementById("createLeadForm_dataSourceId");
		Select dropdown1=new Select(source);
		dropdown1.selectByIndex(1);
		WebElement Marketing = createlead.findElementById("createLeadForm_marketingCampaignId");
		Select dropdown2=new Select(Marketing);
		dropdown2.selectByValue("CATRQ_AUTOMOBILE");
		createlead.findElementById("createLeadForm_firstNameLocal").sendKeys("Romanlocal");
		createlead.findElementById("createLeadForm_lastNameLocal").sendKeys("reignslocal");
		createlead.findElementById("createLeadForm_personalTitle").sendKeys("personal_title");
		createlead.findElementById("createLeadForm_generalProfTitle").sendKeys("general_proftitle");
		createlead.findElementById("createLeadForm_departmentName").sendKeys("IT_dept");
		createlead.findElementById("createLeadForm_annualRevenue").sendKeys("10000");
		WebElement Preferredcurrency = createlead.findElementById("createLeadForm_currencyUomId");
		Select dropdown3=new Select(Preferredcurrency);
		dropdown1.selectByIndex(2);
		WebElement Industry = createlead.findElementById("createLeadForm_industryEnumId");
		Select dropdown4=new Select(Industry);
		dropdown4.selectByIndex(2);
		createlead.findElementById("createLeadForm_numberEmployees").sendKeys("1000");
		WebElement Ownership = createlead.findElementById("createLeadForm_ownershipEnumId");
		Select dropdown5=new Select(Ownership);
		dropdown5.selectByIndex(2);
		createlead.findElementById("createLeadForm_sicCode").sendKeys("ocsms");
		createlead.findElementById("createLeadForm_tickerSymbol").sendKeys("tic_symbol");
		createlead.findElementById("createLeadForm_description").sendKeys("Description_Desc1234");
		createlead.findElementById("createLeadForm_importantNote").sendKeys("Importantnote");
		createlead.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("91");
		createlead.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("5454545444");
		createlead.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("044");
		createlead.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Admin");
		createlead.findElementById("createLeadForm_primaryEmail").sendKeys("roman@gmail.com");
		createlead.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.google.com");
		createlead.findElementById("createLeadForm_generalToName").sendKeys("modi");
		createlead.findElementById("createLeadForm_generalAttnName").sendKeys("Singh");
		createlead.findElementById("createLeadForm_generalAddress1").sendKeys("Line1");
		createlead.findElementById("createLeadForm_generalAddress2").sendKeys("Line2");
		createlead.findElementByName("generalCity").sendKeys("chennai");
		WebElement State = createlead.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select dropdown6=new Select(State);
		dropdown6.selectByIndex(2);
		createlead.findElementById("createLeadForm_generalPostalCode").sendKeys("613066");
		/*WebElement country = createlead.findElementById("createLeadForm_generalCountryGeoId");
		Select dropdown7=new Select(country);
		dropdown7.selectByIndex(2);*/
		createlead.findElementByName("generalPostalCodeExt").sendKeys("3423");
		createlead.findElementByName("submitButton").click();
		createlead.close();
	}

}
