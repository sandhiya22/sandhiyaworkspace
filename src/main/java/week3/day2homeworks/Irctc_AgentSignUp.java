package week3.day2homeworks;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

//Test case: open irctc, Open Agent login , Open sign up , Enter all fields except captcha, Add appropriate wait
//after pincode field

public class Irctc_AgentSignUp {

	public static WebDriverWait wait;
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		//To tell the system that where chrome driver is available
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//object creation for chromedriver class to access the chrome driver
		ChromeDriver browser=new ChromeDriver();
		//To maximize the browser
		browser.manage().window().maximize();
		//Implicit Wait
		browser.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//To open the URL
		browser.get("https://www.irctc.co.in");
		//To click the agent login by using locators
		browser.findElement(By.linkText("AGENT LOGIN")).click();
		//To open sign up link
		browser.findElement(By.linkText("Sign up")).click();
		//Below lines are about to enter sign up form using the id
		browser.findElement(By.id("userRegistrationForm:userName")).sendKeys("LittleJohn90");
		browser.findElement(By.id("userRegistrationForm:password")).sendKeys("Dinesh123");
		browser.findElement(By.id("userRegistrationForm:confpasword")).sendKeys("Dinesh123");
		WebElement SecurityQ = browser.findElement(By.id("userRegistrationForm:securityQ"));
		Select dropdown1=new Select(SecurityQ);
		dropdown1.selectByIndex(1);
		browser.findElement(By.id("userRegistrationForm:securityAnswer")).sendKeys("pet");
		browser.findElement(By.id("userRegistrationForm:firstName")).sendKeys("Yuvi");
		browser.findElement(By.id("userRegistrationForm:middleName")).sendKeys("Middle");
		browser.findElement(By.id("userRegistrationForm:lastName")).sendKeys("Singh");
		browser.findElement(By.id("userRegistrationForm:gender:0")).click();
		browser.findElement(By.id("userRegistrationForm:maritalStatus:1")).click();
		WebElement DOBDD = browser.findElement(By.id("userRegistrationForm:dobDay"));
		Select dropdown2=new Select(DOBDD);
		dropdown2.selectByIndex(1);
		WebElement DOBMM = browser.findElement(By.id("userRegistrationForm:dobMonth"));
		Select dropdown3=new Select(DOBMM);
		dropdown3.selectByIndex(1);
		WebElement DOBYY = browser.findElement(By.id("userRegistrationForm:dateOfBirth"));
		Select dropdown4=new Select(DOBYY);
		dropdown4.selectByIndex(1);
		WebElement Occupation = browser.findElement(By.id("userRegistrationForm:occupation"));
		Select dropdown5=new Select(Occupation);
		dropdown5.selectByIndex(1);
		browser.findElement(By.id("userRegistrationForm:uidno")).sendKeys("242424242423");
		browser.findElement(By.id("userRegistrationForm:idno")).sendKeys("42weqw2424");
		WebElement Country = browser.findElement(By.id("userRegistrationForm:countries"));
		Select dropdown6=new Select(Country);
		dropdown6.selectByIndex(1);
		browser.findElement(By.id("userRegistrationForm:email")).sendKeys("tte@gmail.com");
		browser.findElement(By.id("userRegistrationForm:mobile")).sendKeys("2312312321");
		WebElement Nationality = browser.findElement(By.id("userRegistrationForm:nationalityId"));
		Select dropdown7=new Select(Nationality);
		dropdown7.selectByIndex(1);
		browser.findElement(By.id("userRegistrationForm:address")).sendKeys("13");
		browser.findElement(By.id("userRegistrationForm:street")).sendKeys("Taylor's street");
		browser.findElement(By.id("userRegistrationForm:area")).sendKeys("Greams Road");
		browser.findElement(By.id("userRegistrationForm:pincode")).sendKeys("613006", Keys.TAB);
		Thread.sleep(2000);
		/*wait =new WebDriverWait(browser, 10); 
		wait.until(ExpectedConditions.elementToBeClickable(browser.findElement(By.id("userRegistrationForm:cityName"))));*/
		WebElement city = browser.findElement(By.id("userRegistrationForm:cityName"));
		Select sel = new Select(city);
		sel.selectByVisibleText("Thanjavur");
		Thread.sleep(2000); 
		WebElement Postoffice = browser.findElement(By.id("userRegistrationForm:postofficeName"));
		/*wait.until(ExpectedConditions.elementToBeClickable(city));*/
		Select sele = new Select(Postoffice);
		sele.selectByVisibleText("Marungulam B.O");
		browser.findElement(By.id("userRegistrationForm:landline")).sendKeys("242442343");
		
 
	}     
 
}
