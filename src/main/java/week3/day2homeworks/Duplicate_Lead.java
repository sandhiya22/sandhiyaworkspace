package week3.day2homeworks;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Duplicate_Lead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		//To open the URL
        driver.get("http://leaftaps.com/opentaps");//enter the username
        driver.findElementById("username").sendKeys("DemoSalesManager");
        //enter the password
        driver.findElementById("password").sendKeys("crmsfa");
        //To click login
        driver.findElementByClassName("decorativeSubmit").click();
        //To open crm/sfa
        driver.findElementByLinkText("CRM/SFA").click();
        //To open Leads
        driver.findElementByXPath("//a[text()='Leads']").click();
        //To open find leads
        driver.findElementByXPath("//a[text()='Find Leads']").click();
        //To click on emailtab 
        driver.findElementByLinkText("Email").click();
        //To click on email textbox
        driver.findElementByName("emailAddress").sendKeys("roman@gmail.com");
        //To click on find leads
        driver.findElementByXPath("//button[text()='Find Leads']").click();
        Thread.sleep(2000);
        /*WebDriverWait wait=new WebDriverWait(driver, 30);
        WebElement links = driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
        wait.until(ExpectedConditions.elementToBeClickable(links));
     driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]").click();*/
     //links.click();
        /*wait.until(ExpectedConditions.elementToBeClickable(driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")));
        driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();*/
        driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]").click();
        String text = driver.findElementById("viewLead_firstName_sp").getText();
        driver.findElementByLinkText("Duplicate Lead").click();
        String title = driver.getTitle();
        if(title.contains("Duplicate Lead")) {
        	System.out.println("Title verified successfullys");
        	}else {
        	System.out.println("Title does not matched");
	}
        driver.findElementByClassName("smallSubmit").click();
        String text2 = driver.findElementById("viewLead_firstName_sp").getText();
        if (text.equals(text2)) {
			System.out.println("name matched");
		} else {
			System.out.println("name does not matched");
		}	
        
        driver.close();
	}

}
