package week3.day2classworks;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Erail_Asc {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://erail.in/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.id("txtStationFrom")).clear();
		
		driver.findElement(By.id("txtStationFrom")).sendKeys("MS",Keys.TAB);
		
		driver.findElement(By.id("txtStationTo")).clear();
		
		driver.findElement(By.id("txtStationTo")).sendKeys("TJ",Keys.TAB);
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		WebElement checkbox = driver.findElement(By.id("chkSelectDateOnly"));
		
		if(checkbox.isSelected()) {
			checkbox.click();
		}
		
		driver.findElement(By.linkText("Train Name")).click();
		
		WebElement Table = driver.findElement(By.xpath("//table[@class='DataTable TrainList']"));
		
		List<WebElement> Rows = Table.findElements(By.tagName("tr"));
		
		//system.out.println(Rows.size);	
		
		for(int i=0; i< Rows.size();i++) {
			WebElement eachrow = Rows.get(i);
			
			List<WebElement> Allcell = eachrow.findElements(By.tagName("td"));
			
			System.out.println(Allcell.get(1).getText());
		}
		
		//To capture the screenshot
		
		File printscrn = driver.getScreenshotAs(OutputType.FILE);
		
		//To store in location
		
		File destination= new File("./screenshots/refer1.png");
		
		FileUtils.copyFile(printscrn, destination);
		
		

	}

}
