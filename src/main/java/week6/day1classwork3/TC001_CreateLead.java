package week6.day1classwork3;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
public class TC001_CreateLead extends ProjectMethods{
	
	@BeforeClass(groups="config")
	public void setData() {
		testcaseName ="TC001_CL";
		testDesc ="Create a new lead in leaftaps";
		author ="Dinesh";
		category = "Smoke";
	}
	//@Test(invocationCount=2,invocationTimeOut=2000)
	//@Test(priority= 5)
    @Test(groups="reg")
	public void createlead() {
	//login();	
	WebElement elecrmlink = locateElement("linktext", "CRM/SFA");
	click(elecrmlink);
	WebElement elecreatelead = locateElement("linktext", "Create Lead");
	click(elecreatelead);
	WebElement elecmpname = locateElement("id", "createLeadForm_companyName");
	type(elecmpname, "Edunexv.01");
	WebElement elefname = locateElement("id", "createLeadForm_firstName");
	type(elefname, "Joe1");
	WebElement elelname = locateElement("id", "createLeadForm_lastName");
	type(elelname, "Anoe1");
	WebElement dropdownsource = locateElement("createLeadForm_dataSourceId");
	selectDropDown(dropdownsource, "1", "index");
	WebElement dropdownmarketing = locateElement("createLeadForm_marketingCampaignId");
	selectDropDown(dropdownmarketing, "1", "index");
	WebElement elefnamelocal = locateElement("createLeadForm_firstNameLocal");
	type(elefnamelocal, "joelocal");
	WebElement eleLnamelocal = locateElement("createLeadForm_lastNameLocal");
	type(eleLnamelocal, "Anoelocal");
	WebElement elePTitle = locateElement("createLeadForm_personalTitle");
	type(elePTitle, "personal_title");
	WebElement eleGProfileTitle = locateElement("createLeadForm_generalProfTitle");
	type(eleGProfileTitle, "general_proftitle");
	WebElement eleDeptName = locateElement("createLeadForm_departmentName");
	type(eleDeptName, "IT_dept");
	WebElement eleAnnualRevenue = locateElement("createLeadForm_annualRevenue");
	type(eleAnnualRevenue, "10000");
	WebElement dropdownPreferreCurrency = locateElement("createLeadForm_currencyUomId");
	selectDropDown(dropdownPreferreCurrency, "2", "index");
	WebElement dropdownIndustury = locateElement("createLeadForm_industryEnumId");
	selectDropDown(dropdownIndustury, "2", "index");
	WebElement eleNoEmployee = locateElement("createLeadForm_numberEmployees");
	type(eleNoEmployee, "1000");
	WebElement dropdownOwnerShip = locateElement("createLeadForm_ownershipEnumId");
	selectDropDown(dropdownOwnerShip, "2", "index");
	WebElement elesicCode = locateElement("createLeadForm_sicCode");
	type(elesicCode, "ocsms");
	WebElement eleTickersymbol = locateElement("createLeadForm_tickerSymbol");
	type(eleTickersymbol, "tic_symbol");
	WebElement eleDesc = locateElement("createLeadForm_description");
	type(eleDesc, "Description_Desc1234");
	WebElement eleImpNote = locateElement("createLeadForm_importantNote");
	type(eleImpNote, "Importantnote");
	WebElement elePrimaryPcode = locateElement("createLeadForm_primaryPhoneAreaCode");
	type(elePrimaryPcode, "91");
	WebElement elePrimaryphno = locateElement("createLeadForm_primaryPhoneNumber");
	type(elePrimaryphno, "5454545444");
	WebElement elePrimaryphext = locateElement("createLeadForm_primaryPhoneExtension");
	type(elePrimaryphext, "044");
	WebElement elePrimaryPAFN = locateElement("createLeadForm_primaryPhoneAskForName");
	type(elePrimaryPAFN, "Admin");
	WebElement elePrimaryEmail = locateElement("createLeadForm_primaryEmail");
	type(elePrimaryEmail, "anoe@gmail.com");
	WebElement elePrimaryURL = locateElement("createLeadForm_primaryWebUrl");
	type(elePrimaryURL, "www.google.com");
	WebElement eleGeneralName = locateElement("createLeadForm_generalToName");
	type(eleGeneralName, "Kurt");
	WebElement eleGeneralAtName = locateElement("createLeadForm_generalAttnName");
	type(eleGeneralAtName, "Stephenie");
	WebElement eleGeneralAddr1 = locateElement("createLeadForm_generalAddress1");
	type(eleGeneralAddr1, "Los Angles");
	WebElement eleGeneralAddr2 = locateElement("createLeadForm_generalAddress2");
	type(eleGeneralAddr2, "chris church");
	WebElement eleCity = locateElement("name", "generalCity");
	type(eleCity, "chennai");
	WebElement dropdownGeneralSPG = locateElement("createLeadForm_generalStateProvinceGeoId");
	selectDropDown(dropdownGeneralSPG, "2", "index");
	WebElement eleGeneralPostalCode = locateElement("createLeadForm_generalPostalCode");
	type(eleGeneralPostalCode, "613066");
	WebElement eleGeneralPostalcodeext = locateElement("name", "generalPostalCodeExt");
	type(eleGeneralPostalcodeext, "3434");
	WebElement elesbutton = locateElement("name", "submitButton");
	click(elesbutton);
	WebElement elefindleads = locateElement("xpath", "//a[text()='Find Leads']");
	click(elefindleads);
	WebElement elefirstname = locateElement("xpath", "(//input[contains(@class, 'text')])[29]");
	type(elefirstname, "Joe1");
	WebElement elefindbutton = locateElement("xpath", "//button[text()='Find Leads']");
	click(elefindbutton);
    sleep();
    WebElement eleFlink = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a");
    String text = getText(eleFlink);
    verifyExactText(eleFlink, text);
	//closeBrowser();
	}
}


