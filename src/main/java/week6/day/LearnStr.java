package week6.day;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class LearnStr {
//Question: Input 186553 output 13568
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter a number");
		String str = sc.nextLine();
		char[] chararr = str.toCharArray();
		Set<Character> set= new LinkedHashSet<>();
		List<Character> ls = new ArrayList<>();
		for (char c : chararr) {
			set.add(c);
		}
		ls.addAll(set);
		//Collections.sort(ls);
		System.out.println(ls);
		/*for (Character character : ls) {
			System.out.print(character);
		}*/
		sc.close();
	}

}
