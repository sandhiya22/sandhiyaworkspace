package week6.day;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;



public class FixMyBugs {

	
			public static void main(String []args) throws InterruptedException {

				// launch the browser and disable Notifications
				System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
				
				ChromeOptions op=new ChromeOptions();
				op.addArguments("--disable-notifications");
				ChromeDriver driver = new ChromeDriver(op);
				driver.get("https://www.myntra.com/");

				// Mouse Over on Men
				Actions builder = new Actions(driver);
				builder.moveToElement(driver.findElementByLinkText("Men")).perform();
				
				Thread.sleep(2000);

				// Click on Jackets
				driver.findElementByLinkText("Jackets & Coats").click();


				// Find the count of Jackets
				String leftCount = 
						driver.findElementByXPath("//div[@class='title-container']/span")
						.getText()
						.replaceAll("//D", "");
				System.out.println(leftCount);


				// Click on Jackets and confirm the count is same
				driver.findElementByXPath("//span[@class='categories-num']/following-sibling::div").click();

				// Wait for some time
				Thread.sleep(5000);

				// Check the count
				String rightCount = 
						driver.findElementByXPath("//label[@class='common-customCheckbox vertical-filters-label']/span")
						.getText()
						.replaceAll("//D", "");
				System.out.println(rightCount);

				// If both count matches, say success
				if(leftCount.equals(rightCount)) {
					System.out.println("The count matches on either case");
				}else {
					System.err.println("The count does not match");
				}

				// Click on Offers
				//driver.findElementByXPath("//h3[text()='Offers']").click();

				// Find the costliest Jacket
				List<WebElement> productsPrice = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
				List<String> onlyPrice = new ArrayList<String>();

				for (WebElement productPrice : productsPrice) {
					onlyPrice.add(productPrice.getText().replaceAll("//D", ""));
				}

				// Sort them 
				String max = Collections.max(onlyPrice);

				// Find the top one
				System.out.println(max);
				

				// Print Only Allen Solly Brand Minimum Price
				driver.findElementByXPath("//span[@class='vertical-filters-header']/following::span[5]").click();
				driver.findElementByXPath("//input[@class='filter-search-inputBox']").sendKeys("Allen Solly");
				Thread.sleep(2000);
				driver.findElementByXPath("//div[@class='common-checkboxIndicator']/following::div[4]").click();
				Thread.sleep(2000);

				// Find the costliest Jacket
				List<WebElement> allenSollyPrices = driver.findElementsByXPath("//span[@class='product-discountedPrice']");

				List<String>onlyPrice1 = new ArrayList<String>();

				for (WebElement productPrice : allenSollyPrices) {
					String text = productPrice.getText();
					String replaceAll = text.replaceAll("//D", "");
					onlyPrice1.add(replaceAll);
				}

				// Get the minimum Price 
				String min = Collections.min(onlyPrice1);

				// Find the lowest priced Allen Solly
				System.out.println(min);
				
				driver.close();


			}

		}

	