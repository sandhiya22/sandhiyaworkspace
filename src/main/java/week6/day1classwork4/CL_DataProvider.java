package week6.day1classwork4;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;




public class CL_DataProvider extends ProjectMethods {
	@BeforeClass(groups="config")
	public void setData() {
		testcaseName ="TC001_CL";
		testDesc ="Create a new lead in leaftaps";
		author ="Dinesh";
		category = "Smoke";
	}
	//@Test(invocationCount=2,invocationTimeOut=2000)
	//@Test(priority= 5)
    //@Test(groups="reg")
	@Test(dataProvider="fetchdata")
	public void createlead(String cname,String fname,String lname,String verify) {
	//login();	
	WebElement elecrmlink = locateElement("linktext", "CRM/SFA");
	click(elecrmlink);
	WebElement elecreatelead = locateElement("linktext", "Create Lead");
	click(elecreatelead);
	WebElement elecmpname = locateElement("id", "createLeadForm_companyName");
	type(elecmpname, cname);
	WebElement elefname = locateElement("id", "createLeadForm_firstName");
	type(elefname, fname);
	WebElement elelname = locateElement("id", "createLeadForm_lastName");
	type(elelname, lname);
	WebElement elesbutton = locateElement("name", "submitButton");
	click(elesbutton);
	WebElement elefindleads = locateElement("xpath", "//a[text()='Find Leads']");
	click(elefindleads);
	WebElement elefirstname = locateElement("xpath", "(//input[contains(@class, 'text')])[29]");
	type(elefirstname, verify);
	WebElement elefindbutton = locateElement("xpath", "//button[text()='Find Leads']");
	click(elefindbutton);
    sleep();
    WebElement eleFlink = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a");
    String text = getText(eleFlink);
    verifyExactText(eleFlink, text);
	//closeBrowser();
	}
    @DataProvider(name="fetchdata")
    public String[][] getdata(){
    	String[][] data=new String[2][4];
    	data[0][0]="CTS";
    	data[0][1]="Dinesh";
    	data[0][2]="M";
    	data[0][3]="Dinesh";
    	
    	data[1][0]="Virtusa";
    	data[1][1]="Sandhiya";
    	data[1][2]="S";
    	data[1][3]="Sandhiya";
		return data; 	
    }
    }


