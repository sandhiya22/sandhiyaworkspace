package week2.day2clswork;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

////Identify the occurrence of the character from the given string using the map.Ex. 1 with for loop
public class Mapfunction {

	static int count = 1;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String S = "Testleaf";
		char[] ch = S.toCharArray();
		Map<Character, Integer> Name = new HashMap<>();
		for (int i = 0; i < ch.length; i++) {
			if(Name.containsKey(ch[i]))//key is character so checking
			{
				count++;
				
			}else {
				
				count=1;
			}
			
			Name.put(ch[i], count);
			
			
		}

		for (Entry<Character, Integer> keyandvalue : Name.entrySet()) {
			System.out.println(keyandvalue.getKey() + " ---> " + keyandvalue.getValue());
		
	}
	}
}

/*Ex.2

String S = "Testleaf";
char[] ch = S.toCharArray();
Map<Character, Integer> occurence = new LinkedHashMap<Character, Integer>();
for (char c : ch) {
	
	if(occurence.containsKey(c)) {
		occurence.put(c, occurence.get(c)+1);
	}
	else
		occurence.put(c, 1);
	}

System.out.println(occurence);*/



