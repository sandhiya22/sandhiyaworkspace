package week2.day1clswork;

import java.util.ArrayList;
import java.util.List;
//Question: Identify the occurance of the character in the given string
public class OccuranceOfCharacter {

	public static void main(String[] args) {
		
	String data ="TestLeafee";
	
	char[] ch =data.toCharArray();
	 int count =0;
	for (char c : ch) {
		if (c==data.charAt(1)) {
			count++;
		}
	}
	System.out.println(count);  
	
	/*List Learning is in commented lines
	 * 
	 * List <String> ls =new ArrayList<>();
	ls.add("Bhuvana");
	ls.add("Dinesh");
	ls.add("Bhuvana");
	ls.add("Dinesh");
	ls.add("Sravan"); 
	ls.add("Sakthi");
	System.out.println(ls.size());
	System.out.println(ls.contains("Dinesh"));
	System.out.println(ls.get(4));
	System.out.println(ls.isEmpty());
	System.out.println(ls.remove("Dinesh"));
	System.out.println();
*/
	} 

}













