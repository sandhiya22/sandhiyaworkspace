package wdmethods;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static String[][] getexcelname(String excelname) throws IOException {

		XSSFWorkbook mybook=new XSSFWorkbook("./data/"+excelname+".xlsx");
		XSSFSheet sheet = mybook.getSheetAt(0);
		int rowcount = sheet.getLastRowNum();
        System.out.println(rowcount);  
        short colcount = sheet.getRow(0).getLastCellNum();
        System.out.println(colcount);
        String[][] data=new String[rowcount][colcount];
		for (int i = 1; i <= rowcount; i++) {
			XSSFRow row= sheet.getRow(i);
			for (int j = 0; j < colcount; j++) {
				XSSFCell cell = row.getCell(j);
				String CellValue = cell.getStringCellValue();
				data[i-1][j]=CellValue;
				System.out.println(CellValue);
			}
			
		}
		return data;
       
		}
        
}
