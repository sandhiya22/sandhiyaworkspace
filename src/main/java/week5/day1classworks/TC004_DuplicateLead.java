package week5.day1classworks;

import org.etsi.uri.x01903.v13.impl.GenericTimeStampTypeImpl;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;
import wdmethods.SeMethods;
//Question: Create login steps as method with before method and remove the login steps from all test cases
public class TC004_DuplicateLead extends ProjectMethods{
	
	@Test
	public void DuplicateLead() {
    //login();
	WebElement elecrmlink = locateElement("linktext", "CRM/SFA");
	click(elecrmlink);
	WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
	click(eleLeads);
	WebElement eleFindleads = locateElement("xpath", "//a[text()='Find Leads']");
	click(eleFindleads);
	WebElement eleEmailTab = locateElement("linktext", "Email");
	click(eleEmailTab);
	WebElement eleEmailtextbox = locateElement("name", "emailAddress");
	type(eleEmailtextbox, "anoe@gmail.com");
	WebElement eleFleadsB1 = locateElement("xpath", "//button[text()='Find Leads']");
	click(eleFleadsB1);
	sleep();
	WebElement eleFrecord = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
	click(eleFrecord);
	WebElement eleFirstname = locateElement("viewLead_firstName_sp");
	String text1 = getText(eleFirstname);
	WebElement eleDuplicateLeadB = locateElement("linktext", "Duplicate Lead");
	click(eleDuplicateLeadB);
	verifyTitle("Duplicate Lead | opentaps CRM");
	WebElement eleCreateLeadB = locateElement("class", "smallSubmit");
	click(eleCreateLeadB);
	WebElement eleFname = locateElement("viewLead_firstName_sp");
	String text2 = getText(eleFname);
	if (text1.equals(text2)) {
		System.out.println("name matched");
	} else {
		System.out.println("name does not matched");
	}	
	//closeBrowser();
}
}
