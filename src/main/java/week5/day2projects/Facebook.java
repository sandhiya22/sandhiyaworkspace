package week5.day2projects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Facebook {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		 System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 ChromeOptions options=new ChromeOptions();
		 options.addArguments("--disable-notifications");
	     ChromeDriver driver=new ChromeDriver(options);
	     driver.get("https://www.facebook.com");
	     driver.manage().window().maximize();
	     driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	     driver.findElementById("email").sendKeys("dinesh_heartbeats@yahoo.com");
	     driver.findElementById("pass").sendKeys("******");
	     driver.findElementByXPath("//label[@id='loginbutton']/input").click();
	     driver.findElementByXPath("//input[@name='q']").sendKeys("Testleaf");
	     driver.findElementByXPath("//button[@data-testid='facebar_search_button']").click();
	     WebElement element = driver.findElementByXPath("//div[text()='TestLeaf']");
	     if(element.isDisplayed()) {
	    	 System.out.println("Testleaf is displayed");
	     }else {
	    	 System.out.println("Testleaf is not displayed");
	     }
	     Thread.sleep(2000);
	     String text = driver.findElementByXPath("//button[text()[contains(.,'Like')]][1]").getText();
	     System.out.println(text);
         if(text.equals("Like")) {
        	driver.findElementByXPath("//button[@class='_42ft _4jy0 PageLikeButton _4jy3 _517h _51sy']").click();
        	Thread.sleep(2000);
        	driver.findElementByXPath("//div[text()='TestLeaf']").click();
         }else if(text.equals("Liked")) {
        	 System.out.println("testleaf already liked");
        	 driver.findElementByXPath("//div[text()='TestLeaf']").click();
         }else
        	 System.out.println("Like and liked button not there");
	     
	     Thread.sleep(2000);
	     String title = driver.getTitle();
	     System.out.println(title);
	     if(title.contains("TestLeaf")){
         System.out.println("Tilte contains testleaf");  	
	     } else {
	    	 System.out.println("Title does not contain testleaf");
	     }
	     String replaceAll = driver.findElementByXPath("//div[contains(text(), 'people like this')]").getText().replaceAll("//D", "");
	     System.out.println(replaceAll);
	     driver.close();
	
	     
}
}
	
	
