package week5.day2projects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ZoomCar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	     ChromeDriver driver=new ChromeDriver();
	     driver.get("https://www.zoomcar.com/chennai");
	     driver.manage().window().maximize();
	     driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
         driver.findElementByXPath("//div[@class='newHomeSearch']").click();
         driver.findElementByXPath("//div[@class='component-popular-locations']/div[4]").click();
         driver.findElementByClassName("proceed").click();
         driver.findElementByXPath("//div[@class='days']/div[2]").click();
         driver.findElementByClassName("proceed").click();
         String text = driver.findElementByXPath("//div[text()[contains(.,'Sat')]]").getText();
         System.out.println(text);
         if(text.contains("SAT")) {
        	 System.out.println("Text matched.Start date confirmed");
         }else
        	 System.out.println("text not matched.Start date not confirmed");
         driver.findElementByClassName("proceed").click();
         
         List<WebElement> Results = driver.findElementsByXPath("//div[@class='price']");
         int size = Results.size();
         System.out.println("No of results :"+size);
         List<String> list = new ArrayList<>();
         for (WebElement ele : Results) {
        	 String price = ele.getText();
        	 String finalPrice = price.replaceAll("\\D", "");
        	 list.add(finalPrice);
		} 
         Collections.sort(list,Collections.reverseOrder());
         //Collections.max(list);
         //Collections.reverse(list);
         String amnt = list.get(0);
         System.out.println(amnt);
         if(driver.findElementByXPath("//div[text()[contains(.,'"+amnt+"')]]").getText().contains(amnt)){
         String text2 = driver.findElementByXPath("//div[text()[contains(., '"+amnt+"')]]/preceding::h3[1]").getText();
         driver.findElementByXPath("//div[contains(text(),'"+amnt+"')]/preceding::h3[1]");		
       
    	 System.out.println(text2);
         } 
        /* if(Results.contains(amnt)) {
        	 String text2 = driver.findElementByXPath("//div[text()[contains(., '"+amnt+"')]]/preceding::h3[1]").getText();
        	 System.out.println(text2);
         }*/
	}

}
