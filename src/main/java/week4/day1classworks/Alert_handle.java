package week4.day1classworks;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class Alert_handle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
        System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
        ChromeDriver driver=new ChromeDriver();
        driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.switchTo().frame("iframeResult");
        driver.findElementByXPath("//button[text()='Try it']").click();
        Alert alert = driver.switchTo().alert();
        alert.sendKeys("Dinesh");
        alert.accept();
        String text = driver.findElementByXPath("//p[@id='demo']").getText();
        if(text.contains("Dinesh")) {
        	System.out.println("text is matched");
        }else
        	System.out.println("Text is not matched");
        	}
}
