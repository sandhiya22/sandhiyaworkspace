package week4.day1homeworks;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
        ChromeDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get("http://leaftaps.com/opentaps");
    	WebElement eleUserName = driver.findElementById("username");
		eleUserName.sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("//table[@id='widget_ComboBox_partyIdFrom']/following-sibling::a").click();
		String Primarywindow = driver.getWindowHandle();
		Set<String> Allwindows = driver.getWindowHandles();
		 List<String> listOfWindow = new ArrayList<>();
		    listOfWindow.addAll(Allwindows);
		    driver.switchTo().window(listOfWindow.get(1));
		    driver.findElementByXPath("//input[@name='id']").sendKeys("10371");
		    driver.findElementByXPath("//button[text()='Find Leads']").click();
		    Thread.sleep(2000);
		    driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();
		    driver.switchTo().window(Primarywindow);
		    Thread.sleep(1000);
		    driver.findElementByXPath("//input[@id='partyIdTo']/following-sibling::a").click();
		    Set<String> Allwindows1 = driver.getWindowHandles();
			 List<String> listOfWindow1 = new ArrayList<>();
			 listOfWindow1.addAll(Allwindows1);
			 driver.switchTo().window(listOfWindow1.get(1));
			 driver.findElementByXPath("//input[@name='id']").sendKeys("10379");
			 driver.findElementByXPath("//button[text()='Find Leads']").click();
			 Thread.sleep(2000);
			 driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();
			 driver.switchTo().window(Primarywindow);
			 driver.findElementByLinkText("Merge").click();
			 Alert alert = driver.switchTo().alert();
			 alert.accept();
			 driver.findElementByLinkText("Find Leads").click();
			 driver.findElementByXPath("//input[@name='id']").sendKeys("10371");
			 driver.findElementByXPath("//button[text()='Find Leads']").click();
				String Errormsg = driver.findElementByClassName("x-paging-info").getText();
				
				if(Errormsg.equals("No records to display")) {
					System.out.println("Lead Merged Successfully");
				}
				else {
					System.out.println("Lead is not merged.Enter Id is available");		
		     	}
				File Source = driver.getScreenshotAs(OutputType.FILE);
				File Destination=new File("./screenshots/refer1.png");
				FileUtils.copyFile(Source, Destination);
				
				driver.close();
			 
			 
			 
			 
			 
			 
		    
		    
        
        

	}

}
