package week4.day2homeworks;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;
import wdmethods.SeMethods;

public class TC003_DeleteLead extends SeMethods {
	@Test
	public void DeleteLead() {
	startApp("chrome", "http://leaftaps.com/opentaps");
	WebElement eleusername = locateElement("id", "username");
	type(eleusername, "DemoSalesManager");
	WebElement elsepassword = locateElement("id", "password");
	type(elsepassword, "crmsfa");
	WebElement Loginbutton = locateElement("class", "decorativeSubmit");
	click(Loginbutton);
	WebElement elecrmlink = locateElement("linktext", "CRM/SFA");
	click(elecrmlink);
	WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
	click(eleLeads);
	WebElement eleFindleads = locateElement("xpath", "//a[text()='Find Leads']");
	click(eleFindleads);
	WebElement elePhonetab = locateElement("xpath", "//span[text()='Phone']");
	click(elePhonetab);
	WebElement elePnofield = locateElement("xpath", "//input[@name='phoneAreaCode']");
	type(elePnofield, "91");
	WebElement elePhoneno = locateElement("xpath", "//input[@name='phoneNumber']");
	type(elePhoneno, "5454545444");
	WebElement eleFindlead = locateElement("xpath", "//button[text()='Find Leads']");
	click(eleFindlead);
	sleep();
	WebElement eleText1 = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
	String ResultingId= getText(eleText1);
	click(eleText1);
	WebElement eleDelete = locateElement("class", "subMenuButtonDangerous");
	click(eleDelete);
	WebElement eleFindleadB = locateElement("linktext", "Find Leads");
	click(eleFindleadB);
	WebElement eleIdfield = locateElement("xpath", "//input[@name='id']");
	eleIdfield.toString();
	type(eleIdfield, ResultingId);
	WebElement elefleadV = locateElement("xpath", "//button[text()='Find Leads']");
	click(elefleadV);
	WebElement eleText2 = locateElement("class", "x-paging-info");
	verifyExactText(eleText2, "No records to display");
	closeBrowser();
	

}
}
