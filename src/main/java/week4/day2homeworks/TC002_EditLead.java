package week4.day2homeworks;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;
import wdmethods.SeMethods;

public class TC002_EditLead extends SeMethods{
	@Test
	public void editlead() {
	startApp("chrome", "http://leaftaps.com/opentaps");
	WebElement eleusername = locateElement("id", "username");
	type(eleusername, "DemoSalesManager");
	WebElement elsepassword = locateElement("id", "password");
	type(elsepassword, "crmsfa");
	WebElement Loginbutton = locateElement("class", "decorativeSubmit");
	click(Loginbutton);
	WebElement elecrmlink = locateElement("linktext", "CRM/SFA");
	click(elecrmlink);
	WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
	click(eleLeads);
	WebElement eleFindleads = locateElement("xpath", "//a[text()='Find Leads']");
	click(eleFindleads);
	WebElement eleFname = locateElement("xpath", "(//input[contains(@class, 'text')])[29]");
	type(eleFname, "joe1");
	WebElement eleFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
	click(eleFindLeads);
	sleep();
	WebElement eleFvalue = locateElement("xpath", "(//a[@class='linktext'])[4]");
	click(eleFvalue);
	sleep();
	WebElement eleEditLead = locateElement("xpath", "(//a[@class='subMenuButton'])[3]");
	click(eleEditLead);
	WebElement eleCmpnameclr = locateElement("xpath", "//input[@id='updateLeadForm_companyName']");
	clear(eleCmpnameclr);
	WebElement eleCompname = locateElement("xpath", "//input[@id='updateLeadForm_companyName']");
	type(eleCompname, "Raw");
	WebElement eleSubmit = locateElement("xpath", "//input[@name='submitButton']");
	click(eleSubmit);
	//Need to confirm the changed name appears
	closeBrowser();
}
}
