package week4.day2classworks;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdmethods.SeMethods;

public class CreateLead_Framework extends SeMethods {
	@Test
	public void createlead() {
		
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleusername = locateElement("id", "username");
		type(eleusername, "DemoSalesManager");
		WebElement elsepassword = locateElement("id", "password");
		type(elsepassword, "crmsfa");
		WebElement Loginbutton = locateElement("class", "decorativeSubmit");
		click(Loginbutton);
		WebElement elecrmlink = locateElement("linktext", "CRM/SFA");
		click(elecrmlink);
		WebElement elecreatelead = locateElement("linktext", "Create Lead");
		click(elecreatelead);
		WebElement elecmpname = locateElement("id", "createLeadForm_companyName");
		type(elecmpname, "Edunexv.01");
		WebElement elefname = locateElement("id", "createLeadForm_firstName");
		type(elefname, "Joe");
		WebElement elelname = locateElement("id", "createLeadForm_lastName");
		type(elelname, "Anoe");
		WebElement elesbutton = locateElement("name", "submitButton");
		click(elesbutton);
		WebElement elefindleads = locateElement("xpath", "//a[text()='Find Leads']");
		click(elefindleads);
		WebElement elefirstname = locateElement("xpath", "(//input[contains(@class, 'text')])[29]");
		type(elefirstname, "Joe ");
		WebElement elefindbutton = locateElement("xpath", "//button[text()='Find Leads']");
		click(elefindbutton);
	    sleep();
	    WebElement eleFlink = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a");
	    getText(eleFlink);
	    verifyExactText(eleFlink, "joe");
		closeBrowser();
		
	
		
		
		
		

	
	

	}

}
