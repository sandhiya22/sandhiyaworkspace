package week1.day2learnings;

import java.util.Scanner;

public class SumOfAddNo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the number");
		
		int input = scan.nextInt();
		
		int sum=0;
		
		while(input>0) {
			int num= input%10;
			
			if (num%2!=0) {
				sum=sum+num;
			}
		
		input =input/10;
		}
		
		
		System.out.println(sum);
		

	}

}
