package week1.day1learnings;

public class MyCycle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//syntax to call a class
				//Classname refname = new Classname();
				Bicycle cycle = new Bicycle();
				//to call a method or variable
				//refname.methodName(); -> method
				//refname.varName;
				String cycleColor = cycle.getCycleColor();
				System.out.println(cycleColor);
				
				boolean hasBasket = cycle.hasBasket;
				System.out.println(hasBasket);
	}

}
